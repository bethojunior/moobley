<?php

    class ClienteController extends View{

    function actionClient()
    {
        $js = ['modulos/modalVideo'];
        $css = ['reset','navbar','vendor/materialize.min','assets','reset','customers','menuMobile','footer','preload','modalVideo'];

        $views = ['preload/index','nav/navBar','nav/navBarMobile','customers/index','import/footer'];

        $seo = new stdClass();
        $seo->description = 'Aplicativo gratuito para Mobilidade Urbana, único app preocupado com o Meio Ambiente e oferecendo segurança e preço baixo para seus usuário. Baixe e conheça.';
        $seo->title = 'MOOBLEY | Aplicativo de Mobilidade Urbana |';

        $this->layoutBuilder($views, $js, $css, $seo);
    }

    function actionPreRegister(){
        $js = ['controllers/ClienteController','modulos/cliente/init'];
        $css = ['cliente/preRegister'];

        $views = ['client/preRegister'];

        $seo = new stdClass();
        $seo->description = 'Pre cadastro da MOOBLEY';
        $seo->title = 'MOOBLEY | Aplicativo de Mobilidade Urbana |';

        $this->layoutBuilder($views, $js, $css, $seo);
    }
}