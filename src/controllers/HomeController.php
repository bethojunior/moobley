<?php 
    class HomeController extends View{

        function actionIndex()
        {

            $js  = ['utils/navBar','utils/navbarHomeMobile','modulos/home'];
            $css = ['assets','reset','home','navbarTransparent','menuMobile','menuMobileHome','footer','preload'];
            $views = ['preload/index','nav/navBarTransparent','nav/navBarMobileHome','home/index','import/footer'];

            $seo = new stdClass();
            $seo->description   = 'O real app de mobilidade verde, oferecendo o melhor preço sem tarifa dinâmica e com segurança.';
            $seo->title         = 'Moobley. Aplicativo de Mobilidade Urbana';

            $this->layoutBuilder($views, $js, $css, $seo);
        }
    }