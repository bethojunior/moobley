<?php
/**
 * Created by PhpStorm.
 * User: Fabrica704_Acer
 * Date: 02/01/2019
 * Time: 16:06
 */

class TripController extends View
{
    public function actionGet()
    {
        $this->layoutBuilder(
            ['trip/share'],
            [
                'controllers/TripController',
                'service/MapService',
                'modulos/trip/share',
            ],
            ['trip/share']);
    }
}