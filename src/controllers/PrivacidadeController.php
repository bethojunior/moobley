<?php
    class PrivacidadeController extends View{

    function actionPrivacy()
    {
        $css = ['reset','navbar','vendor/materialize.min','assets','privacy'];

        $views = ['preload/index','nav/navBarOrange','nav/navBarMobile','politics/index'];

        $seo = new stdClass();
        $seo->description = 'Aplicativo gratuito para Mobilidade Urbana, único app preocupado com o Meio Ambiente e oferecendo segurança e preço baixo para seus usuário. Baixe e conheça.';
        $seo->title = '';

        $this->layoutBuilder($views, $js, $css, $seo);
    }
}