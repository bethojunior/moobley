<div class="generalPreRegisterCliente">
    <img id="promotion" src="<?php echo Host::getLocal() ?>webfiles/img/logoMoobley.jpeg">

    <div class="divInputs container-form">
        <div class="thisDivInput">
            <label class="thisInput">Nome</label>
            <input placeholder="" id="name" autocapitalize="words">
        </div>
        <div class="thisDivInput">
            <label class="thisInput">Email</label>
            <input placeholder="" type="email" id="email">
        </div>
        <div class="thisDivInput">
            <label class="thisInput">Telefone</label>
            <input placeholder="" type="number" id="telphone">
        </div>
        <div class="thisDivInput">
            <label class="thisInput">Senha</label>
            <input placeholder="" type="password" id="password">
        </div>
        <div class="col s12 text-description">Após finalizar seu cadastro, você será
            redirecionado para o download do app
        </div>
        <button class="btn" id="sendData">CONCLUIR</button>
    </div>
</div>

<div class="modal" id="chooseApp">
    <div class="contentModal">
        <button class="btnAndroid"><a style="text-decoration: none; color: white"
                                      href="https://play.google.com/store/apps/details?id=br.com.taxireturn.client&showAllReviews=true">Baixar
                para android</a></button>
        <br>
        <button class="btnIos"><a style="text-decoration: none; color: white;"
                                  href="https://itunes.apple.com/br/app/return-cliente/id1452297278?mt=8">Baixar para
                IOS</a></button>
    </div>
</div>