<title>Viagem Compartilhada</title>
<div class="container-map" id="map">
</div>
<div class="container-destination">
    <div>
        <span id="addressOrigin"></span>
        <p><span id="timeArrivalOrigin"></span>&nbsp;<span>Embarcando</span></p>
    </div>
    <div>
        <span id="addressDestination"></span>
        <p><span id="timeArrivalDestination"></span>&nbsp;<span id="status">Encerrada</span></p>
    </div>
</div>
<div class="container-data-driver">
    <footer class="container-advertisement">
        <img src="/webfiles/img/logo/logo.webp">
        <span>Use você também, baixe agora e ganhe um desconto exclusivo.</span>
        <a href="https://play.google.com/store/apps/details?id=br.com.taxireturn.client">baixar</a>
    </footer>
    <div id="profileDriver"></div>
    <p>
        <span id="name"></span>
        <span id="plate"></span>
    </p>
</div>
<div class="modal-finish">
    <div>
        <h4>Viagem Encerrada</h4>
        <p>Use você também, baixe agora e ganhe um desconto exclusivo.</p>
        <a href="https://play.google.com/store/apps/details?id=br.com.taxireturn.client">baixar</a>
    </div>
</div>