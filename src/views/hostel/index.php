<div id="main-hostel">
    
    <img src='<?= Host::getLocal(); ?>webfiles/img/background/baner_site_recepcionista.jpg' class='img-resposive-hero' alt='Sistema para hoteis e pousadas em fortaleza'/>
    <img src='<?= Host::getLocal(); ?>webfiles/img/background/banner_hostel_mobile.jpg' class='img-resposive-hero-mobile'  />
    
    <h1><strong>SISTEMA</strong> DE TÁXI PARA <br>
    <strong>HOTÉIS</strong> E <strong>POUSADAS</strong>. <br>
    VOCÊ GANHA DINHEIRO <br>
    PEDINDO TÁXI!</h1>

    <a href='<?=Host::getHostPwa();?>' class='btn-call-taxi-hostel'>PEDIR UM TÁXI</a>
</div>

<div class='container-hostel'>
    
    <h2 class='describ-text'>
    O Moobley é um <strong>sistema gratuito para hotéis e pousadas</strong>, voltado especificamente para a <strong>gestão</strong> do serviço de táxi em seu estabelecimento. Queremos oferecer para seus <strong>hóspedes</strong>, o conforto e a segurança do táxi tradicional no retorno para rodoviária e aeroporto, aliando comunicação e tecnologia em um <strong>software</strong> pensado com base nas necessidades da sua <strong>hotelaria</strong>.</h2>

    <div class='row-because-taxi-return'>
        <div class='descript-because'>
            <h2>Porque o <br/> Moobley?</h2>
            <p >Desenvolvemos uma cadeia onde todos  saem ganhando. Nosso objetivo é incentivar  que o trabalho
                coletivo   gere lucros para todas as partes
            </p>
        </div> 
        <div class='box-mobile'>
            <img src='<?= Host::getLocal(); ?>webfiles/img/cityscape.png'  />
            <h2 class='align-text-center-img'>Bom para o <br/> hotel/pousada</h2>
        </div> 
        <div class='box-mobile'>
            <img src='<?= Host::getLocal(); ?>webfiles/img/piggy-bank.png'  />
            <h2 class='align-text-center-img'>Bom para o  <br/> bolso</h2>
            
        </div>
        <div class='box-mobile'>
            <img src='<?= Host::getLocal(); ?>webfiles/img/idea.png'  />
            <h2 class='align-text-center-img'>Bom para  <br/> o funcionário</h2>
        </div>
    </div>
</div>
<div class='div-box-absolute bg-grey'>
        <div class='bg-dark-yellow'>
            <img src='<?= Host::getLocal(); ?>webfiles/img/background/software-para-hoteis-e-pousadas.png' class='img-lange'  alt='software para hoteis e pousadas'/>
            <div class='descript-box-absolute '>
                <p>Você já estava acostumado a pedir táxis para 
                    seus hóspedes, agora você pode ganhar dinheiro com isso. 
                    É muito simples, basta cadastrar o seu hotél e começar a faturar. 
                    Sempre que pedir um táxi usando o Moobley  você garante até 13% do valor da corrida para seu hotél.</p>
                     <div class="wrapper-btn">
                        <a class="btn-section-ahref-about" id='openModalEstablishment'>Saiba mais</a>
                         <a ><img src="<?= Host::getLocal(); ?>webfiles/img/icons/right-arrow.png" alt=""></a>

                    </div>
            </div>
            
        </div>
    </div>
<div class='container-hostel'>
    <div class='inf-start-now'>
        <p>Conheça o verdadeiro significado de <b>juntar o últil ao agradável.</b></p>
        <a class='btn-comece-agr gradient-yellow' href='<?=Host::getHostPwa();?>'>
            Começar agora
        </a>
    </div>
</div> 
<img src='<?= Host::getLocal(); ?>webfiles/img/hero-img-hostel-pc.jpg' class='hero-hostel-home' />
<img src='<?= Host::getLocal(); ?>webfiles/img/hero-home-mobile.jpg' class='hero-hostel-home-mobile'  />
