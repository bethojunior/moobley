<!-- defaults js -->
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/lib/jquery-3.2.1.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/lib/axios.min.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/lib/jquery.mask.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/lib/jquery.inputmask.bundle.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/lib/load-image.all.min.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/plugins/sweetalert.min.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/vendor/materialize/js/materialize.js"></script>

<!-- utils -->
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/service/ConnectionServer.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/utils/Host.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/utils/ConnectAPI.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/utils/navbarMobile.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/utils/SimpleSwall.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/utils/ElementProperty.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/utils/validateData.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/utils/SwalCustom.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/utils/preload.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/utils/windowResize.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/utils/Mask.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/utils/FormGroup.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/utils/Session.js"></script>
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/service/Autocomplete.js"></script>

<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1356935,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<!--controllers-->
<script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/controllers/MailchimpController.js"></script>
    <script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/modulos/mailchimp.js"></script>

<!-- import dynamic js in each page  -->
<?php if(isset($this->filesJs)) : ?>
    <?php foreach ($this->filesJs as $file) : ?>
        <script type="text/javascript" src="<?php echo Host::getLocal(); ?>webfiles/js/<?php echo $file ?>.js"></script>
    <?php endforeach; ?>
<?php endif; ?>
</body>