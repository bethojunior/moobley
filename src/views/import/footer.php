<footer class="footer">
    <div class='footer-container'>
        <div class='collun-container'>
            <img src='<?= Host::getLocal(); ?>webfiles/img/logoMoobley.jpeg' width='70px' />
            <p>504 Norte HM 01 Alameda 02 Lote 1 Casa 2<br/> Palmas - To</p>
        </div>
        <div class='collun-container'>
            <p><b>Contatos:</b></p>
            <p><a href="tel:63981335814">(63)9 8133-5814</p></a>
            <!-- <br/> -->
            <p><b>Suporte:</b></p>
            <p><a href='https://api.whatsapp.com/send?phone=5563981335814&text=Olá,%20meu%20amigo!.'>(63)9 8133-5814 (WhatsApp)</p></a>
            <a href="mailto:contato@moobley.com.br"><p>appmoobley@gmail.com</p></a>
        </div>
<!--        <div class='collun-container'>-->
<!--            <div class="first">-->
<!--                <p>Estamos em : </p>-->
<!--                <label>São Gonçalo do amarante - Ce</label><br>-->
<!--                <label>Juazeiro do Norte - Ce</label><br>-->
<!--                <label>Maracanaú - Ce</label><br>-->
<!--                <label>Barbalha - Ce</label><br>-->
<!--                <label>Crato - Ce</label><br>-->
<!--                <label>Sobral - Ce</label><br>-->
<!--                <label>Iguatu - Ce</label>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class='collun-container'>-->
<!--            <div class="first">-->
<!--                <p>Em breve : </p>-->
<!--                <label>Quixeramobim - Ce</label><br>-->
<!--                <label>Fortaleza - Ce</label><br>-->
<!--                <label>Manaus - Ce</label><br>-->
<!--                <label>Mossoró - Ce</label><br>-->
<!--                <label>Quixadá - Ce</label><br>-->
<!--            </div>-->
<!--        </div>-->
    </div>
    <div class='copy'>
        <p>2019 Moobley - Todos os direitos reservados.</p>
    </div>
</footer>
