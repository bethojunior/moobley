<div class="container-receipt">
    <h4>Solicitação de transfêrencia</h4>
    <p id="date"></p>
    <div>
        <label for="value">Valor</label>
        <input type="text" id="value" value="" disabled="">
    </div>
    <!--<div>
        <label for="payer">Pagador</label>
        <p id="payer">Heghbertho Gomes Costa</p>
    </div>-->
    <div>
        <label for="agency">Agência</label>
        <p id="agency"></p>
    </div>
    <div>
        <label for="account">Conta</label>
        <p id="account"></p>
    </div>
    <div>
        <i class="fas fa-barcode"></i>
        <b>Documento</b>
    </div>
    <div>
        <label for="favored">Favorecido</label>
        <p id="favored">PM FORTALEZA</p>
    </div>
    <!--<div>
        <label for="code">Código do boleto</label>
        <p>8455641512321345454324356465614223233231324364854</p>
    </div>-->
    <footer>
        <b>Código de autenticação</b>
        <p id="codeConfirmation"></p>
        <label>
            Return  
        </label>
    </footer>
</div>