<div class='nav-wrapper'>
    <nav class='nav-bar-option' id='navbar'>
        <div class="container">
            <div class='brand-icon'>
                <a href="<?= Host::getLocal() ?>"><img src="<?= Host::getLocal(); ?>webfiles/img/logoMoobley.jpeg" class='brand-logo left'/></a>
            </div>
            <ul class='right hide-on-med-and-down'>
                <li><a href='<?= Host::getLocal(); ?>Plus/plus'>PLUS</a></li>
                <li><a href='<?= Host::getLocal(); ?>Cliente/client'>Para você</a></li>
                <li><a href='<?= Host::getLocal(); ?>Hotelaria/sistema-para-hoteis-e-pousadas'>Turismo</a></li>
                <li><a href='<?= Host::getLocal(); ?>Taxista/aplicativo-taxista-em-fortaleza'>Taxista</a></li>
                <li><a href='<?=Host::getHostPwa();?>'>Empresas</a></li>
                <li><a href='<?=Host::getHostPwa();?>'>Entrar</a></li>
                <li class="quero-dirigir"><a href='https://play.google.com/store/apps/details?id=br.com.taxireturn.driver' >Quero dirigir</a></li>
            </ul>
        </div>
    </nav>
</div>
