<nav class="navBlog row">
    <div class="index col l4">
        <div class="col l2">
            <img class="responsive-img logoHeader" src="<?= Host::getLocal();?>webfiles/img/eco_return.png">
        </div>
        <span class="opacityIndex"><span style="font-weight: 550">Return</span> <span style="font-weight: 350">Blog</span></span>
    </div>
</nav>
 <div class="mtNav">

     <div class="row">
         <div class="generalBlog">
             <div class="col l1"></div>
             <div class="col l10" id="mainBlog">
                 <div class="col l5">
                     <img id="imageMainBlog" class="responsive-img" src="http://www.miseria.com.br/fotos_not/2018/10/13/20181013160025_8921_capa.jpg">
                 </div>
                 <div class="col l7">
                     <p class="descriptionMainBlog">
                         <b class="titleMainBlog">Return é ecológico</b>
                         <br>
                         <span>De acordo com Heghbertho , o taxista desembarca um turista que veio do aeroporto em uma pousada no Cariri. Em vez de o motorista retornar com o automóvel vazio ao seu ponto, os estabelecimentos hoteleiros próximos (cadastrados) podem utilizar a ferramenta e solicitar um táxi para esse hóspede, gerando assim novas corridas.</span>
                     </p>
                 </div>
             </div>
             <div class="col l1"></div>
         </div>
     </div>
     <div class="row">
         <div class="col l1"></div>
         <div class="col l10">
             <div class="divider"></div>
             <div class="panelBestsPosts" id="panelBestsPosts">
                 <div class="bestPost col l4">
                     <div class="col l12">
                         <div class="col l12">
                             <img class="responsive-img" src="http://blogs.diariodonordeste.com.br/cariri/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-10-at-13.02.30-1-1024x576.jpeg">
                         </div>
                         <span class="descriptioFirstPost">
Genuinamente cearense, a TaxiReturn está sediada em Fortaleza. Há um ano trabalha em prol da melhoria da mobilidade urbana, da sustentabilidade e do desenvolvimento econômico e turístico.  A companhia possui um sistema de gestão de frota de táxis no Aeroporto de Fortaleza
                    </span>
                     </div>
                 </div>

                 <div class="bestPost col l4">
                     <div class="col l12">
                         <div class="col l12">
                             <img class="responsive-img" src="http://www.waldineypassos.com.br/wp-content/uploads/2017/03/Juazeiro-do-Norte-Padre-Cicero-horto.jpg">
                         </div>
                         <span class="descriptioFirstPost">
                         Juazeiro do Norte.<br>
                        Foi lançado no último domingo (14), em Juazeiro do Norte, a plataforma de mobilidade TaxiReturn, que vai beneficiar taxistas e setor hoteleiro do Município e, também, de Crato e Barbalha. O Cariri é o primeiro local que recebe a ferramenta que nasceu sobre os pilares da inovação, economia e sustentabilidade.
                    </span>
                     </div>
                 </div>

                 <div class="bestPost col l4">
                     <div class="col l12">
                         <div class="col l12">
                             <img class="responsive-img" src="https://i.ytimg.com/vi/vLojEggV6Lc/maxresdefault.jpg">
                         </div>
                         <span class="descriptioFirstPost">
                        O Plus te dá a vantagem de trabalhar em todos os lugares da cidade, não precisa ficar preso em um só local, você ainda contribui para diminuição de poluentes, ajudando assim o planeta. Baixe o aplicativo e comece a ganhar dinheiro.
                    </span>
                     </div>
                 </div>
             </div>
         </div>
         <div class="col l1"></div>
     </div>

     <div class="row">
         <div class="thirdPartBlog">
             <div class="col l1"></div>
             <div class="col l10">
                 <div class="col l12">
                     <div class="col l3">
                         <span class="title">Mais populares</span>
                         <div class="divider"></div>
                         <div id="morePopular" class="morePopular">
                             <div class="col l12 divPopular">
                                 <div class="col l8">
                                     A return salva o planeta. Com a return você diminui em 80% de emissão de poluentes no ambiente.
                                 </div>
                                 <div class="col l4">
                                     <img class="responsive-img" src="<?= Host::getLocal();?>webfiles/img/eco_return.png">
                                 </div>
                             </div>
                             <div class="col l12 divPopular">
                                 <div class="col l8">
                                     A return entrou em Portugal! Solicite sua viagem com desconto
                                 </div>
                                 <div class="col l4">
                                     <img class="responsive-img" src="<?= Host::getLocal(); ?>webfiles/img/logo/logo.webp">
                                 </div>
                             </div>
                             <div class="col l12 divPopular">
                                 <div class="col l8">
                                     Tarifa inicial de corrida baixa em até 20% nas horas de rush
                                 </div>
                                 <div class="col l4">
                                     <img class="responsive-img" src="https://taxireturn.com.br/webfiles/img/logo.jpg">
                                 </div>
                             </div>
                         </div>
                     </div>
                     <div class="col l9">
                         <div class="carousel carousel-slider">
                             <a class="carousel-item" href="#one!"><img class="responsive-img" src="https://scontent.ffor13-1.fna.fbcdn.net/v/t1.0-9/44434296_2228403534063599_2909133954063794176_n.png?_nc_cat=100&_nc_ht=scontent.ffor13-1.fna&oh=037002f544e7422de4fab8915086cc4a&oe=5CBC55FE"></a>
                             <a class="carousel-item" href="#two!"><img class="responsive-img" src="https://lorempixel.com/800/400/food/2"></a>
                             <a class="carousel-item" href="#three!"><img class="responsive-img" src="https://lorempixel.com/800/400/food/3"></a>
                             <a class="carousel-item" href="#four!"><img class="responsive-img" src="https://lorempixel.com/800/400/food/4"></a>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="col l1"></div>
         </div>
     </div>

     <div class="row">
         <div class="fourthBlog">
             <div class="col l1"></div>
             <div class="col l10">
                 <div class="col l3">
                     <div class="col l12">
                         <img class="responsive-img"  src="http://www.miseria.com.br/fotos_not/2018/10/13/20181013160025_8921_capa.jpg">
                     </div>
                     <div class="col l12 aboutSubtitle">
                         <span class="subtilte">
                             Economize com a return, os melhores preços são aqui.
                         </span>
                     </div>
                 </div>

                 <div class="col l3">
                     <div class="col l12">
                         <img class="responsive-img"  src="http://www.miseria.com.br/fotos_not/2018/10/13/20181013160025_8921_capa.jpg">
                     </div>
                     <div class="col l12 aboutSubtitle">
                         <span class="subtilte">
                             Quando a demanda por carros está muito alta, o Uber sobe as tarifas automaticamente para que sempre haja motoristas disponíveis: quem estiver disposto a pagar mais, consegue um veículo na hora; quem não estiver, espera até o preço baixar. O multiplicador da tarifa dinâmica é sempre bem sinalizado na interface do aplicativo, mas isso está prestes a mudar.
                         </span>
                     </div>
                 </div>


                 <div class="col l3">
                     <div class="col l12">
                         <img class="responsive-img" src="http://www.miseria.com.br/fotos_not/2018/10/13/20181013160025_8921_capa.jpg">
                     </div>
                     <div class="col l12 aboutSubtitle">
                         <span class="subtilte">
                            Procurando onde viajar??? Nordeste é o lugar mais pesquisado para viagens nessas férias. Com suas belas praias e clima perfeito para fugir da rotina, O noderste ganha foco nessas férias
                         </span>
                     </div>
                 </div>



             </div>
             <div class="col l1"></div>
         </div>
     </div>




 </div>