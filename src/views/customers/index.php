<meta name="viewport" content="width=device-width, initial-scale=1.0">
<div id="main-customers">
    
    <img src='<?= Host::getLocal(); ?>webfiles/img/background/banner_customers.jpg' class='img-resposive-hero''/>
    <img src='<?= Host::getLocal(); ?>webfiles/img/background/banner-customers-mobile.jpg' class='img-resposive-hero-mobile'  />

    <h1><strong>APLICATIVO</strong> DE MOBILIDADE<br>
        URBANA PREOCUPADO<br>
        <strong>COM VOCÊ</strong> E COM O <strong>MEIO<br>
        AMBIENTE<strong>!</h1>

    <a href='<?=APP_CLIENT?>' class='btn-donwload-app'>Baixe Moobley</a>
</div>
<div class="bg-line-orange">
</div>

<div class='container-customer'>
    
    <h2 class='describ-text'>
        O Moobley é um aplicativo de mobilidade urbana. É o único no seguimento que tem como uma de suas prioridades o Meio Ambiente, oferecendo um modo de diminuir a poluição, porém fornecendo todo conforto possivel com um preço justo e fixo sem sustos. Baixe o app e ajude o planeta.
    </h2>
    <div class='row-because-taxi-return'>
        <div class='descript-because'>
            <h2>QUAIS OS BENEFÍCIOS<br/> DE USAR O MOOBLEY?</h2>
            <p>Desenvolvemos um modo prático e rápido de mobilidade<br>
                urbana, onde fosse possível emitir a menor quantidade de<br>
                CO2. transformando todo km rodado em km útil, usando a MOOBLEY<br>
                você estará contribuindo com a melhoria do meio ambiente.
            </p>
        </div> 
        <div class='box-mobile'>
            <img src='<?= Host::getLocal(); ?>webfiles/img/eco_return.png'  />
            <h2 class='align-text-center-img'>Bom para o <br/> meio ambiente</h2>
        </div> 
        <div class='box-mobile'>
            <img src='<?= Host::getLocal(); ?>webfiles/img/porquinho.png'  />
            <h2 class='align-text-center-img'>Bom para o  <br/> bolso</h2>
            
        </div>
    </div>
</div>
<div class="container-middle">
</div>
<div class='div-box-absolute bg-grey'>
        <div class='bg-dark-yellow'>
            <img src='<?= Host::getLocal(); ?>webfiles/img/background/city.png' class='img-lange'/>
            <div class='descript-box-absolute'>
                Mobilidade urbana é coisa séria. Meio ambiente também. <br> O aplicativo MOOBLEY é o ÚNICO a se preocupar com as questões ambientais. Ao solicitar uma corrida, o cliente sabe a quantidade de CO2 que deixou de ser emitido caso utiliza-se outra plataforma. Além da preservação, o usuário tem à disposição PREÇO BAIXO, PREÇO FIXO e JUSTO durante as corridas. Ajude a salvar o planeta.
                    <div class="wrapper-btn">
                         <a href='https://play.google.com/store/apps/details?id=br.com.taxireturn.client' class='btn-donwload-app-right'>Baixar agora</a>

                    </div>
            </div>
        </div>
</div>
