<meta name="viewport" content="width=device-width, initial-scale=1.0">
<div style="margin: 0;padding: 0;">

<div id="main-customers">
    
    <img src='<?= Host::getLocal(); ?>webfiles/img/background/fundo_Site_Plus.jpg' class='img-resposive-hero''/>
    <img src='<?= Host::getLocal(); ?>webfiles/img/background/Fundo_Site_Plus_MOBILE.jpg' class='img-resposive-hero-mobile'  />

    <h1>Seja um <strong>Moobley</strong> e ganhe<br>
        dinheiro dirigindo seu<br>
        carro particular e tenha<br>
        sua liberdade financeira.</h1>

    <a target="_blank" href='https://play.google.com/store/apps/details?id=br.com.moobley.driver' class='btn-donwload-app'>Torne-se Moobley</a>
</div>
<div class="bg-line-orange">
</div>

<div class='container-customer'>
    <h2 class='describ-text'>
        O <strong>Moobley</strong> é um aplicativo de mobilidade urbana, que chega com intuito de ajudar
        você a ter uma renda extra, use seu carro particular como ferramenta de trabalho e seja
        seu próprio patrão, fazendo seu horário e salário. Com o <strong>PLUS</strong>, você pode faturar
        até R$ 300,00 por dia.Faça seu cadastro agora e comece a trabalhar hoje mesmo.
    </h2>

    <div class='row-because-taxi-return'>
        <div class='descript-because'>
            <h2>ESCOLHA O HORÁRIO QUE VAI TRABALHAR E<br> O QUANTO QUER GANHAR</h2><br>
        </div>
    </div>
    <div class='row-because-taxi-return'> 
        <div class='box-mobile'>
            <img src='<?= Host::getLocal(); ?>webfiles/img/SacoDinheiro.png'  />
            <h2 class='align-text-center-img'><b>Ganhe dinheiro</b><br>Você pode dirigir e ganhar<br> o quanto você quiser.<br>Quanto mais você dirigir,<br> mais você ganhará.</h2>
        </div> 
        <div class='box-mobile'>
            <img src='<?= Host::getLocal(); ?>webfiles/img/clock.png'  />
            <h2 class='align-text-center-img'><b>Defina seu próprio horário</b><br>Dirija somente quando for<br> melhor para você.<br>Sem chefe cobrando você.<br>Isso significa que pode<br> começar e parar quando preferir.</h2>
        </div>
        <div class='box-mobile'>
            <img src='<?= Host::getLocal(); ?>webfiles/img/cell.png'  />
            <h2 class='align-text-center-img'><b>A inscrição é fácil</b><br>É fácil.<br>Cadastre-se hoje e<br> comece a dirigir<br> imediatamente.<br>apenas 5 minutos .</h2>
        </div>
    </div>
</div>
<div class="container-middle">
</div>
<div class='div-box-absolute bg-grey'>
        <div class='bg-dark-yellow'>
            <img src='<?= Host::getLocal(); ?>webfiles/img/background/city-out.png' class='img-lange'/>
            <div class='descript-box-absolute'>
            <h5>O <strong>Moobley</strong> te dá a vantagem de trabalhar em todos os lugares da cidade, não precisa ficar
            preso em um só local, você ainda contribui para diminuição de poluentes, ajudando assim
            o planeta. Baixe o aplicativo e comece a ganhar dinheiro.</h5>
                    <div class="wrapper-btn">
                         <a href='<?=APP_CLIENT?>' class='btn-donwload-app-right'>Baixar agora</a>

                    </div>
            </div>
        </div>
</div>

</div>