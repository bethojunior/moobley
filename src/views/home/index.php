<section class="main-image-home" id="home-bg">
    <img src="<?= Host::getLocal(); ?>webfiles/img/background/imgNav.jpg" alt="Ganhe 13% sob todas as corridas que pedir" class='desktop-img'>
    <img src="<?= Host::getLocal(); ?>webfiles/img/background/cabecalhol_Mobile.jpg" alt="Ganhe 13% sob todas as corridas que pedir" class='mobile-img'>
<!--    <h1>Nosso dinâmico <br/> é andar com<br> preço fixo.<br> Barato, sempre.</h1>-->
    <h1>Na MOOBLEY <br>você anda com<br> preço fixo e <br> barato sempre</h1>
    <a href='<?=APP_CLIENT?>' class='btn-call-taxi button-big-shadow'>CHAMAR MOOBLEY</a>
</section>

<div class='container-home' id='features'>
    <h2 class='describ-text'>O <strong>MOOBLEY</strong> é um <strong>Aplicativo de mobilidade urbana</strong> que promove uma parceria direta entre <strong>passageiros</strong> e <strong>motoristas</strong>. Nosso objetivo principal é oferecer a melhor experiência de mobilidade urbana aos <strong>passageiros</strong> durante o percurso, trazendo ainda, uma série de vantagens para os seus usuários.</h2>
    <div class='detalhes-types'>
        <div class='detalhes-item'>
            <h2>Motorista</h2>
            <div class="block-features">
                <div class="img-preview">
                    <img src='<?= Host::getLocal(); ?>webfiles/img/taxista.png'  class='img-item'/>
                </div>
                <div class="block-features-content">
                    <p>Garanta uma corrida de volta ao seu <br/> ponto de partida e fature ainda mais</p>
                    <div class='btn-center'>
                        <a href='<?= Host::getLocal(); ?>plus/plus' class='btn-saiba-mais'>Saiba mais</a>
                    </div>
                </div>
            </div>
        </div>
        <div class='detalhes-item'>
            <h2>Passageiro</h2>
            <div class="block-features">
                <div class="img-preview">
                    <img src='<?= Host::getLocal(); ?>webfiles/img/passageiro.png'  class='img-item'/>
                </div>
                <div class="block-features-content">
                    <p>Voce usuário também pode ganhar dinheiro com a Moobley indicando seus amigos e ganhando por cada corrida que ele fizer</p>
                    <div class='btn-center'>
                        <a href='<?= Host::getLocal(); ?>Cliente/client' class='btn-saiba-mais'>Saiba mais</a>
                    </div>
                </div>
            </div>
        </div>
        <div class='detalhes-item'>
            <h2>Empresas</h2>
            <div class="block-features">
                <div class="img-preview">
                    <img src='<?= Host::getLocal(); ?>webfiles/img/icon_hotel.png'  class='img-item'/>
                </div>
                <div class="block-features-content">
                    <p>Ganhe até 13% sobre todas as <br/> corridas que pedir para seus hóspedes</p>
                    <div class='btn-center'>
                        <a class='btn-saiba-mais' onmouseover="overTxt('Saiba mais',this);" onmouseout="outTxt('EM BREVE',this);">Saiba mais</a>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="midSite">
    <div class="bodyMid">
        <span class="title">
            A <b>MOOBLEY </b> é um aplicativo de <br> Mobilidade verde
        </span>
        <br>
        <div class="mt-2">
            <label>Mobilidade verde é poder ir e vir mais barato sem agredir <br> o meio ambiente e ainda ajudar a salvar o <b>PLANETA</b></label>
        </div>
        <br>
        <button class="btnKnow">Quero conhecer</button>
    </div>
    <div class="imgMid">
        <img src="<?= Host::getLocal(); ?>webfiles/img/carro_estrada.png">
    </div>
</div>
<!--<div class="page-turista">-->
<!--    <div class='descript-page-turista'>-->
<!--        <p class='describ-viajantes-turista'>VIAJANTES & TURISTA</p>-->
<!--        <h2 class='title-page-turista'>Táxi mais barato que <br> qualquer aplicativo.</h2>-->
<!--        <p class='describ-bottom-page-turista'>Volte a viajar com a segurança do táxi <br>pagando menos do que você imagina.</p>-->
<!--        <a -->
<!--            class='btn-quero-conhecer  gradient-yellow button-compact-shadow' -->
<!--            id='mouserHouverChangeTxt'-->
<!--            onmouseover="overTxt('EM BREVE',this);" -->
<!--            onmouseout="outTxt('Quero conhecer',this);"> Quero conhecer</a>-->
<!--    </div>-->
<!--   <div class='brand-image'>-->
<!--       <img src="--><?//= Host::getLocal(); ?><!--webfiles/img/foto_turista.png" class='img-turista'>-->
<!--   </div>-->
<!--</div>-->
<!--<div class='container-home'>-->
<!--    <div class='div-hoteis-pousadas'>-->
<!--        <div class='inf-ganhe-mais-gold'>-->
<!--            <p class='label-title'>Hoteis & Pousadas</p>-->
<!--            <h2 class='tile-hoteis-pousadas'>Seu hotel ganha,  você ganha! <br/></h2>-->
<!--            <div class="block-hostel">-->
<!--                <img src="--><?//= Host::getLocal(); ?><!--webfiles/img/planta-money.png" alt="" class='img-hoteis-pousadas'>-->
<!--                <div class="block-hostel-content">-->
<!--                    <h3 class='title-da-img-money'>ganhe mais dinheiro</h3>-->
<!--                    <p>Com o <strong>Return</strong> você e o seu hotel ganham uma porcentagem sobre todas as <strong>corridas de táxi</strong> que pedir para seus hóspedes pelo app com destino à rodoviária ou aeroporto.</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class='image-return'>-->
<!--            <img src="--><?//= Host::getLocal(); ?><!--webfiles/img/return.png" alt="" class='imge-return-img'>-->
<!--        </div>-->
<!--        <div class='inf-ganhe-mais-gold convesion-box-flex'> -->
<!--        -->
<!--            <div class="block-hostel">-->
<!--                <img src="--><?//= Host::getLocal(); ?><!--webfiles/img/conversation.png" alt="" class='img-hoteis-pousadas'>-->
<!--                <div class="block-hostel-content">-->
<!--                    <h3 class='title-da-img-money'>Experência do hóspede</h3>-->
<!--                    <p>Garanta que a experiência final do hóspede no seu hotel seja tão boa quanto toda a estadia. O <strong>Return</strong> direciona para os seus clientes, <strong>táxis credenciados</strong> com a rodoviária e aeroporto da cidade, E trazendo <strong>segurança</strong> e <strong>qualidade</strong> no bagageiro.</p>-->
<!--                </div>-->
<!--            </div>-->
<!--            -->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="btn-hoteis">-->
<!--        <a href='--><?//= Host::getHostPwa(); ?><!--' class='gradient-yellow button-big-shadow'>Cadastrar agora</a>-->
<!--    </div>-->
<!--</div>-->
<div class='div-flex-box-seja-taxista'>
    <img src="<?= Host::getLocal(); ?>webfiles/img/baner_site_motorista.png" class='img-lange-seja-taxista'>
    <div class='div-flex-text-description'>
        <h2>Motorista</h6>
        <h1>Seja um motorista parceiro.</h2>
        <p>Com o MOOBLEY você não paga mensalidade nem percentual</p>
        <a href="<?= Host::getLocal(); ?>plus/plus" class='button-compact-shadow'>Quero Dirigir</a>
    </div>
</div>

