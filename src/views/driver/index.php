<!-- main image -->
<section id='bg-taxi' class='main-image-home'>
    <img src="<?= Host::getLocal();?>webfiles/img/background/banner_taxista.jpg" alt="Banner Taxista desktop" class='responsive-img bg-desktop'>
    <img src="<?= Host::getLocal();?>webfiles/img/background/baner_site_taxista_mobile.jpg" alt="Banner Taxista mobile" class='responsive-img bg-desktop bg-mobile'>
    <h1>Aumente sua renda <br> <Strong>Taxista!</Strong> baixe o <strong>App</strong> <br> e comece a <strong>lucrar</strong> </h1>
    <a href='https://play.google.com/store/apps/details?id=br.com.taxireturn.driver' target='_BLANK' class="btn-section">Começar agora</a>
</section>
<!-- end main image -->

<!-- Step List Numbers -->
<section id='how-to-integrate' class='bg-grey'>
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h2 class='text-section'>Fature mais <strong>taxista!</strong> Dirigindo com o <strong>Aplicativo Moobley</strong> você aumenta a sua renda sem precisar rodar ainda mais pela cidade. Queremos que você aproveite as <strong>viagens</strong> de retorno de hotéis e pousadas, oferecendo à você novas corridas, com destino ao seu posto inicial.</h2>
            </div>
        </div>
        
        <h6 class="subtitle-section">Veja como é fácil</h6>
        
        <div class="time-line-sign-up-numbers timeline-desktop">
            <div class="row">
                <div class="col s12">
                    <ul class="list-numbers">
                        <li class="item-number active-number">
                            1
                            <div class="wrapper-line">                                <hr>

                                <span class="rounded"></span>
                                <div class="text-timeline">
                                    <span>Baixe o APP</span>
                                    <p class="text-timeline" >Acesse a loja e baixe o <br>aplicativo do Moobley</p>
                                </div>
                            </div>
                        </li>
                        <li class="item-number">
                            2
                            <div class="wrapper-line">
                                <span class="rounded"></span>
                                <hr>
                                <div class="text-timeline">
                                    <span>Cadastre-se</span>
                                    <p class="text-timeline" >Faça seu cadastro em menos de 7 <br>minutos e aguarde a confirmação</p>
                                </div>
                            </div>
                        </li>
                        <li class="item-number">
                            3
                            <div class="wrapper-line">
                                <span class="rounded"></span>
                                <hr>
                                <div class="text-timeline">
                                    <span>Fique online</span>
                                    <p class="text-timeline" >Depois que seu cadastro estiver con-<br>firmado você já pode ativar o app.</p>
                                </div>
                            </div>
                        </li>
                        <li class="item-number">
                            4
                            <div class="wrapper-line">
                                <span class="rounded"></span>
                                <hr>
                                <div class="text-timeline">
                                    <span>Aceite corridas</span>
                                    <p class="text-timeline">Com o aplicativo você fica disponível <br> para receber corridas, basta aceita-las.</p>
                                    
                                </div>
                            </div>
                        </li>
                        <li class="item-number">
                            5
                            <div class="wrapper-line">
                                <span class="rounded"></span>
                                <div class="text-timeline">
                                    <span>Ganhe dinheiro</span>
                                    <p class="text-timeline">Pronto. Aceite as corridas e<br> comece à faturar.</p>
                                </div>
                            </div>     
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="timeline-mobile">
                <div class="row">
                    <div class="col s2">
                        <div class="timeline">
                            <div class="block-line-mobile">
                                <div class="row">
                                    <span class='rounded'></span>
                                    <div class="vertical-line"></div>
                                </div>
                            </div>

                            <div class="block-line-mobile">
                                <div class="row">
                                    <span class='rounded'></span>
                                    <div class="vertical-line"></div>
                                </div>
                            </div>

                            <div class="block-line-mobile">
                                <div class="row">
                                    <span class='rounded'></span>
                                    <div class="vertical-line"></div>
                                </div>
                            </div>

                            <div class="block-line-mobile">
                                <div class="row">
                                    <span class='rounded'></span>
                                    <div class="vertical-line"></div>
                                </div>
                            </div>

                            <div class="block-line-mobile">
                                <div class="row">
                                    <span class='rounded'></span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col s10">
                        
                        <div class="block-step-text-mobile">
                            <div class="row">
                                <div class="col s3"><div class="number">1</div></div>
                                <div class="col s9">
                                    <div class="wrapper-block-content">
                                        <div class="title-block">Baixe o App</div>
                                        <div class="text-block">Acesse a loja e baixe o aplicativo do Táxi Moobley</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block-step-text-mobile">
                            <div class="row">
                                <div class="col s3"><div class="number">2</div></div>
                                <div class="col s9">
                                    <div class="wrapper-block-content">
                                        <div class="title-block">Cadastre-se</div>
                                        <div class="text-block">Faça seu cadastro em menos de 7 minutos e aguarde a confirmação.</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block-step-text-mobile">
                            <div class="row">
                                <div class="col s3"><div class="number">3</div></div>
                                <div class="col s9">
                                    <div class="wrapper-block-content">
                                        <div class="title-block">Fique Online</div>
                                        <div class="text-block">Depois que seu cadastro estiver confirmado você já pode ativar o app.</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block-step-text-mobile">
                            <div class="row">
                                <div class="col s3"><div class="number">4</div></div>
                                <div class="col s9">
                                    <div class="wrapper-block-content">
                                        <div class="title-block">Aceite corridas</div>
                                        <div class="text-block">Com o app ativado você fica disponível para receber corridas, basta aceita-las.</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block-step-text-mobile">
                            <div class="row">
                                <div class="col s3"><div class="number">5</div></div>
                                <div class="col s9">
                                    <div class="wrapper-block-content">
                                        <div class="title-block">Ganhe dinheiro</div>
                                        <div class="text-block">Pronto. Aceite as corridas e comece à faturar.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div> <!-- end container -->    
</section>
<!-- End step List Numbers -->

<!-- more -->
<section id="more" class='bg-grey'>
    <div class='wrapper bg-dark-yellow'>
        <div class="container">
            <div class="row">
                <div class="col l6 m12 s12">
                    <img class="main-image" src="<?= Host::getLocal(); ?>webfiles/img/background/taxi-return-em-fortaleza.png" alt="Aplicativo para taxistas">
                </div>
                <div class="col l6 m12 s12">
                    <p class='text-section'>
                    Com o Moobley você garante uma corrida de volta para seu posto inicial quando deixar passageiros nas proximidades de pousadas e hotéis, assim você fatura cerca de 40% a mais, traçando o mesmo percurso de sempre.
                    </p>
                    <div class="wrapper-btn">
                        <a class="btn-section" id='openModalEstablishment'>Saiba mais</a>
                        <a><img src="<?= Host::getLocal(); ?>webfiles/img/icons/right-arrow.png" alt=""></a>
                    </div>
                </div>
            </div>      
        </div> <!-- end container -->
    </div> 
</section>
<!-- end more -->

<!-- signup driver -->
<section id="sign-up-driver" class='bg-grey'>
    <div class="container">
        <div class="row">
            <div class="col l8 m12 s12">
                <p class="subtitle-section">Conheça o verdadeiro significado <br>de <span>juntar o útil ao agradável.</span></p>
            </div>
            <div class="col l4 m12 s12">
                <a href='<?=APP_DRIVER?>' target='_BLANK' class="btn-section gradient-yellow">Quero dirigir</a>
            </div>
        </div>
    </div> <!-- end container -->
</section>
<!-- end signup driver -->

<section id="download-app" class='bg-grey'>
    <div class="container">
        <div class="row col s12">
            <ul class="list-download">
                 <li class="download-item"><a href='<?=APP_DRIVER?>' target='_BLANK'><img src="<?= Host::getLocal(); ?>webfiles/img/icons/google-play.png" alt="Google Play"></a></li>
            </ul>
            <h2 class="subtitle-section">Baixe o App e comece agora!</h2>
        </div>
    </div>
</section>