document.addEventListener("DOMContentLoaded", function () {
    const mapService = new MapService();
    const elementProperty = new ElementProperty();

    const SettingMap = {
        mapActive: false
    };

    const tripController = new TripController();

    const url = location.href.split("/");
    const tripReference = url[url.length - 1];

    updateViewTrip(tripReference);

    function updateViewTrip(tripReference) {
        tripController.share(tripReference).then(({status, data, message}) => {
            if (!status) {
                SimpleSwall.modalError(message);
                return;
            }
            if (!SettingMap.mapActive){
                initMap(data);
                return;
            }

            watchTrip(data);

        });
    }

    function initMap(trip) {
        mapService.importScriptWithPlaces().then(function () {
            SettingMap.mapActive = true;

            mapService.initMap("map", {
                lat: parseFloat(trip.origin.latitude),
                lng: parseFloat(trip.origin.longitude)
            });

            watchTrip(trip);
        });
    }

    function watchTrip(trip) {
        if(trip.status === "finished" || trip.status === "canceled"){
            elementProperty.getElement(".modal-finish",element=>{
                element.classList.add("active");
            });
        }

        getRoute(trip).then(({origin, destination}) => {
            //mapService.showRoute(origin, destination);
            mapService.updateRouteInMap(destination, origin, ({routes}) =>{
                let id = "#timeArrivalOrigin";
                if(trip.status === "in_progress") {
                    elementProperty.getElement(id,element=>{
                        element.innerHTML = "<span>ok</span>";
                    });
                    id = "#timeArrivalDestination"
                }


                elementProperty.getElement(id,element=>{
                    const date = new Date();

                    date.setSeconds(date.getSeconds() + routes[0].legs[0].duration.value);

                    const timeSplit = date.toLocaleString().split(" ")[1].split(":");

                    element.innerHTML = `${timeSplit[0]}:${timeSplit[1]}`;
                });
            })
        });

        elementProperty.getElement("#profileDriver",element=>{
            element.style.backgroundImage = `url(${environment.hosts.profileDriver}${trip.driver.profileImage})`;
        });

        elementProperty.getElement("#name",element=>{
            element.innerHTML = trip.driver.nickname;
        });

        elementProperty.getElement("#plate",element=>{
            element.innerHTML = trip.driver.car.plate;
        });

        elementProperty.getElement("#addressOrigin",element=>{
            element.innerHTML = trip.origin.address;
        });

        elementProperty.getElement("#addressDestination",element=>{
            element.innerHTML = trip.destination.name;
        });

        mapService.addPointClient({
            lat : parseFloat(trip.origin.latitude),
            lng : parseFloat(trip.origin.longitude)
        });
        mapService.addPointDestination({
            lat : parseFloat(trip.destination.latitude),
            lng : parseFloat(trip.destination.longitude)
        });

        setTimeout(function () {
            updateViewTrip(tripReference);
        }, 10000);
    }

    function getRoute(trip) {
        return new Promise((resolve, reject) => {
            const destination = {
                lat: 0, lng: 0
            };
            const origin = {
                lat: parseFloat(trip.driver.location.latitude),
                lng: parseFloat(trip.driver.location.longitude),
            };
            switch (trip.status) {
                case "accepted":
                case "boarding": {
                    destination.lat = parseFloat(trip.origin.latitude);
                    destination.lng = parseFloat(trip.origin.longitude);

                    break;
                }
                case "in_progress": {

                    destination.lat = parseFloat(trip.destination.latitude);
                    destination.lng = parseFloat(trip.destination.longitude);
                    mapService.addPointDestination(destination)
                    break;
                }
                default: {
                    reject();
                    return;
                }
            }

            resolve({origin, destination});
        });
    }


});