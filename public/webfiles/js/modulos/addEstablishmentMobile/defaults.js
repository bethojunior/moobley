const elementProperty         = new ElementProperty()
const countryController       = new CountryController()
const bankController          = new BankController()
const images = []

const establishment = {

    fantasyName: null,
    document: null,
    address: null,
    number: null,
    zipcode: null,
    neighborhood: null,
    cityId: null,
    estateId: null,

    // secondstep
    rating: null,
    minDailyRate: null,
    maxDailyRate: null,
    averageValue: null,
    indicationDiscount: null,


    commercialEmail: null,
    administratorEmail: null,
    commercialPhone: null,
    administratorPhone: null,

    latitude: null,
    longitude: null,
    bankAccount: {
        bankId: null,
        typeAccount: null,
        agency: null,
        numberAccount: null,
    },
    employees: []
}